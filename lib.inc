section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.counter:
    cmp byte [rdi+rax], 0
    je .end
    inc rax
    jmp .counter
.end:    
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rsi
    mov rdx, rax
    mov rax, 1
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
	push rdi
	mov rax, 1
	mov rdi, 1
	mov rsi, rsp
	mov rdx, 1
	syscall
	pop rdi
	ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате S
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    mov rax, rdi
    mov r10, 10
    mov rcx, rsp
    dec rcx
    sub rsp, 24
.loop:
    xor rdx, rdx
    div r10
    dec rcx
    add rdx, '0'
    mov [rcx], dl 
    cmp rax, 0
    je .end 
    jmp .loop
.end:	
    mov rdi, rcx
    call print_string
    add rsp, 24
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jnl .plusnumber
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
.plusnumber:
    call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
.loop:
    xor r10, r10
    xor r11, r11
    mov r10b, byte[rdi+rcx]
    mov r11b, byte[rsi+rcx]
    cmp r10, r11
    jne .notequals
    cmp byte[rdi+rcx], 0
    je .equals
    inc rcx
    jmp .loop	
.equals:
    mov rax, 1
    ret
.notequals:
    xor rax, rax
    ret
; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rdi, rdi
    xor rax, rax
    mov rdx, 1
    push qword 0
    mov rsi, rsp
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор


read_word: 
	xor r9, r9
	mov r10, rdi 
.loop:
	push rdi
	push rsi
	call read_char
	pop rsi
	pop rdi
	cmp r9, 0
	jz .space
.next:
	cmp r9, rsi
	jae .buferr
	cmp rax, 0
	jz .end
	mov [rdi + r9], rax

	cmp rax, 0x9
	jz .end
	cmp rax, 0xa
	jz .end
	cmp rax, 0x20
	jz .end

	inc r9
	jmp .loop

.space:
	cmp rax, 0x9
	jz .loop
	cmp rax, 0xa
	jz .loop
	cmp rax, 0x20
	jz .loop
	jmp .next

.buferr:
	xor rax, rax
	ret
.end:
	xor rax, rax
	mov [rdi + r9], rax
	mov rax, r10
	mov rdx, r9
	ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    xor rdx, rdx
    xor r9, r9
    mov r10, 10 	
.loop:
    mov r9b, byte[rdi + rcx]
    cmp r9b, '0'
    jb .end 
    cmp r9b, '9'
    ja .end
    sub r9b, '0'
    mul r10
    add rax, r9
    inc rcx
    jmp .loop
.end:
    mov rdx, rcx   
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rcx, rcx
    xor rdx, rdx
    xor r9, r9
    mov r10, 10
    push 0
    mov r9b, byte[rdi]
    cmp r9b, '-'
    jne .continue
    mov r8, 1
    mov [rsp], r8
    inc rcx
    jmp .loop  
.continue:
    cmp r9b, '0'
    jb .checknegnumber
    cmp r9b, '9'
    ja .checknegnumber
    sub r9b, '0'
    mul r10
    add rax, r9
    inc rcx
.loop:
    xor r9, r9
    mov r9b, byte[rdi+rcx]
    cmp r9b, '0'
    jb .checknegnumber
    cmp r9b, '9'
    ja .checknegnumber
    sub r9b, '0'
    mul r10
    add rax, r9
    inc rcx
    jmp .loop	  
.checknegnumber:
    pop r9
    cmp r9, 1
    jne .end
    neg rax
.end:
    mov rdx, rcx
    ret
; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
;rdi,rsi,rdx
string_copy:
    cmp rdx, 0
    je .bufzero
    xor r8, r8
    mov r8b, byte[rdi]
    mov byte[rsi], r8b
    inc rdi
    inc rsi
    dec rdx
    cmp r8, 0
    jne string_copy
    ret
.bufzero:
    xor rax, rax
    ret       	
